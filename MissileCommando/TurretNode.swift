//
//  TurretNode.swift
//  MissileCommando
//
//  Created by Brian Rizzo on 11/25/15.
//  Copyright © 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit

class PlayerNode: SKNode {

	var turretRotation:CGFloat = 0
	var turretOffset:CGFloat = 0

	init(position:CGPoint) {
		super.init()
		self.name = "player"
		self.position = position

		let playerBase = SKSpriteNode(imageNamed: "playerBase")
		playerBase.position = CGPoint(x: 0, y: playerBase.size.height/2) // CGPoint(x: 0, y: ground.position.y + (playerBase.size.height/2))
		playerBase.zPosition = 1
		playerBase.physicsBody = SKPhysicsBody(circleOfRadius: playerBase.size.width/2)
		playerBase.physicsBody!.categoryBitMask = BodyType.playerBase.rawValue
		playerBase.physicsBody!.dynamic = false
		self.addChild(playerBase)

		let turret = SKSpriteNode(imageNamed: "turret")
		turret.name = "turret"
		turret.anchorPoint = CGPoint(x: 0.5, y: 0.0)
		turret.position = CGPoint(x: 0, y: playerBase.position.y)
		turret.zPosition = 2

		let target:SKSpriteNode = SKSpriteNode(imageNamed: "target")
		target.position = CGPoint(x: turret.position.x, y: turret.size.height + 50)
		target.zPosition = 3
		turret.addChild(target)

		self.addChild(turret)
	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}

	func rotateTurret(sender:UIRotationGestureRecognizer) {
		let turret = self.childNodeWithName("turret")
		if(sender.state == .Changed) {
			turretRotation = CGFloat( -sender.rotation ) + turretOffset
			let maxRotation:CGFloat = 1.4
			if (turretRotation < -maxRotation) {
				turretRotation = -maxRotation
			} else if (turretRotation > maxRotation) {
				turretRotation = maxRotation
			}

			turret!.zRotation = turretRotation
		}
		if(sender.state == .Ended) {
			turretOffset = turretRotation
		}
	}

}
