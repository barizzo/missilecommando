//
//  EnemyMissile.swift
//  MissileCommando
//
//  Created by Brian Rizzo on 11/29/15.
//  Copyright © 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit

class EnemyMissile: SKNode {

	var missileNode = SKSpriteNode()
	var missileAnimation = SKAction()

	override init() {
		super.init()
		self.name = "enemyMissile"
	}

	required init?(coder aDecoder: NSCoder) {
	    fatalError("init(coder:) has not been implemented")
	}

	func createMissile(image:String) {
		missileNode = SKSpriteNode(imageNamed: image)
		self.addChild(missileNode)

		self.physicsBody = SKPhysicsBody(circleOfRadius: missileNode.size.width/2.25, center:CGPoint(x: 0, y: 0))
		self.physicsBody?.dynamic = true
		self.physicsBody?.affectedByGravity = true
		self.physicsBody?.allowsRotation = false
		self.physicsBody?.categoryBitMask = BodyType.enemyMissile.rawValue
		self.physicsBody?.contactTestBitMask = BodyType.ground.rawValue | BodyType.bullet.rawValue | BodyType.base.rawValue | BodyType.playerBase.rawValue

		// missile animation
		var atlasTextures:[SKTexture] = []
		for var i = 0; i < 10; i++ {
			atlasTextures.insert(SKTexture(imageNamed: "enemyMissile\(i+1)"), atIndex: i)
		}

		let animation = SKAction.animateWithTextures(atlasTextures, timePerFrame: 1.0/20, resize:true, restore:false)
		missileAnimation = SKAction.repeatActionForever(animation)
		missileNode.runAction(missileAnimation, withKey:"animation")

		// missile fire particiles
		if let fireEmmiter = SKEmitterNode(fileNamed: "FireParticles") {
			fireEmmiter.name = "fireEmitter"
			fireEmmiter.zPosition = -1
			fireEmmiter.particleLifetime = 10
			//fireEmmiter.numParticlesToEmit = 200
			fireEmmiter.targetNode = self
			self.addChild(fireEmmiter)
		}
	}

}
