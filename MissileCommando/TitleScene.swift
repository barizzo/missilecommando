//
//  TitleScene.swift
//  MissileCommando
//
//  Created by Brian Rizzo on 11/24/15.
//  Copyright © 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit

class TitleScene: SKScene {

	var isPhone:Bool = true
	var screenWidth:CGFloat = 0
	var screenHeight:CGFloat = 0
	var instructionLabel:SKLabelNode?
	var introImage:SKSpriteNode?

	override func didMoveToView(view: SKView) {
		/* Setup your scene here */
		if (UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
			isPhone = false
		}

		screenWidth = self.view!.bounds.width
		screenHeight = self.view!.bounds.height

		self.backgroundColor = SKColor.blackColor()
		self.anchorPoint = CGPoint(x:0.5, y: 0.0) // middle bottom

		//let introImage:SKSpriteNode = SKSpriteNode(imageNamed: "intro_screen")
		// force the introImage to scale to the device width/height
		let introImageTexture:SKTexture = SKTexture(imageNamed: "intro_screen")
		let introImageSize = CGSize(width: screenWidth, height: screenHeight)
		introImage = SKSpriteNode(texture: introImageTexture, color: SKColor.clearColor(), size: introImageSize)
		introImage!.position = CGPoint(x: 0, y: screenHeight/2)
		self.addChild(introImage!)

		self.createInstrucitonLabel()
	}

	override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		/* Called when a touch begins */
		let transition = SKTransition.revealWithDirection(SKTransitionDirection.Down, duration: 1.0)
		let scene = GameScene(size: self.scene!.size)
		scene.scaleMode = SKSceneScaleMode.AspectFill
		self.scene!.view!.presentScene(scene, transition: transition)

		// Example: fade out elements in the scene
		//let fadeOut = SKAction.fadeAlphaTo(0, duration: 0.8)
		//let remove = SKAction.removeFromParent()
		//let seq = SKAction.sequence([fadeOut,remove])
		//instructionLabel!.runAction(seq)
		//introImage!.runAction(seq)
	}

	override func update(currentTime: CFTimeInterval) {
		/* Called before each frame is rendered */
	}

	func createInstrucitonLabel() {
		instructionLabel = SKLabelNode(fontNamed: "BM germar")
		instructionLabel!.text = "Touch to Begin Game"
		instructionLabel!.horizontalAlignmentMode = .Center
		instructionLabel!.verticalAlignmentMode = .Center
		instructionLabel!.fontColor = SKColor.whiteColor()
		if (isPhone == true) {
			instructionLabel!.position = CGPoint(x: 0, y: screenHeight * 0.15)
			instructionLabel!.fontSize = 30
		} else {
			instructionLabel!.position = CGPoint(x: 0, y: screenHeight * 0.20)
			instructionLabel!.fontSize = 40
		}

		let wait = SKAction.waitForDuration(0.5)
		let fadeOut = SKAction.fadeAlphaTo(0, duration: 0.8)
		let fadeIn = SKAction.fadeAlphaTo(1, duration: 0.2)
		let seq = SKAction.repeatActionForever(SKAction.sequence([wait,fadeOut,fadeIn]))
		instructionLabel!.runAction(seq)

		self.addChild(instructionLabel!)
	}


}
