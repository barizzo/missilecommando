//
//  GameScene.swift
//  MissileCommando
//
//  Created by Brian Rizzo on 11/23/15.
//  Copyright (c) 2015 Brian Rizzo. All rights reserved.
//

import SpriteKit
import AVFoundation

// PhysicsBodies: the two bodies colliding must add to a unique value; to do that, use base-2 numeral system
// E.g. add any two BodyTypes, you'll never get a value used for any other BodyType
enum BodyType:UInt32 {
	case playerBase = 1
	case base = 2
	case bullet = 4
	case enemy = 8
	case enemyMissile = 16
	case enemyBomb = 32
	case ground = 64
}

class GameScene: SKScene {

	var isPhone:Bool = true
	var screenWidth:CGFloat = 0
	var screenHeight:CGFloat = 0

	var gameIsActive:Bool = false
	var instructionLabel = SKLabelNode()

	let loopingBg = SKSpriteNode(imageNamed: "stars")
	let loopingBg2 = SKSpriteNode(imageNamed: "stars")
	let moon = SKSpriteNode(imageNamed: "moon")
	var ground = SKSpriteNode()
	let playerBase = SKSpriteNode(imageNamed: "playerBase")
	let turret:SKSpriteNode = SKSpriteNode(imageNamed: "turret")
	let target:SKSpriteNode = SKSpriteNode(imageNamed: "target")

	let turretTargetRadius:CGFloat = 200
	var turretRotation:CGFloat = 0
	var turretOffset:CGFloat = 0

	var rattleAction = SKAction()

	let rotateRecognizer = UIRotationGestureRecognizer()
	let tapRecognizer = UITapGestureRecognizer()

	var bgAudioPlayer = AVAudioPlayer()

	// MARK: ======== INHERITED

	override func didMoveToView(view: SKView) {
        /* Setup your scene here */
		if (UIDevice.currentDevice().userInterfaceIdiom == .Pad) {
			isPhone = false
		}

		self.screenWidth = self.view!.bounds.width
		self.screenHeight = self.view!.bounds.height

		self.physicsWorld.gravity = CGVector(dx: 0, dy: -0.1)

		rotateRecognizer.addTarget(self, action: "rotatedView:")
		self.view!.addGestureRecognizer(rotateRecognizer)
		tapRecognizer.addTarget(self, action: "tappedView")
		tapRecognizer.numberOfTouchesRequired = 1
		tapRecognizer.numberOfTapsRequired = 1
		self.view!.addGestureRecognizer(tapRecognizer)

		self.backgroundColor = SKColor.blackColor()
		self.anchorPoint = CGPoint(x:0.5, y: 0.0) // middle bottom

		self.createActions()
		self.createInstrucitonLabel()
		self.playBackgroundAudio("levelsound")
		self.buildScene()

		self.startGame()
	}
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
		/* Called when a touch begins */
		let fadeOut = SKAction.fadeAlphaTo(0, duration: 0.8)
		let remove = SKAction.removeFromParent()
		let seq = SKAction.sequence([fadeOut,remove])
		instructionLabel.runAction(seq)
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }

	// MARK: ======== BUILD SCENE / INITIAL SETUP

	func createActions() {
		// Rattle
		let rattleUp = SKAction.moveByX(0, y: 5, duration: 0.05)
		let rattleDown = SKAction.moveByX(0, y: -5, duration: 0.05)
		let rattleSeq = SKAction.sequence([rattleUp,rattleDown])
		self.rattleAction = SKAction.repeatAction(rattleSeq, count: 3)
	}

	func createInstrucitonLabel() {
		instructionLabel = SKLabelNode(fontNamed: "BM germar")
		instructionLabel.text = "Rotate Fingers to Swivel Turret"
		instructionLabel.horizontalAlignmentMode = .Center
		instructionLabel.verticalAlignmentMode = .Center
		instructionLabel.fontColor = SKColor.whiteColor()
		instructionLabel.position = CGPoint(x: 0, y: screenHeight/2)
		instructionLabel.zPosition = 1
		if (isPhone == true) {
			instructionLabel.fontSize = 30
		} else {
			instructionLabel.fontSize = 40
		}

		self.addChild(instructionLabel)

		let wait = SKAction.waitForDuration(0.5)
		let fadeOut = SKAction.fadeAlphaTo(0, duration: 0.8)
		let fadeIn = SKAction.fadeAlphaTo(1, duration: 0.2)
		let remove = SKAction.removeFromParent()
		let seq = SKAction.repeatAction(SKAction.sequence([wait,fadeOut,fadeIn]),count: 3)
		instructionLabel.runAction(SKAction.sequence([seq,wait,remove]))
	}

	func buildScene() {
		// Background
		moon.position = CGPoint(x: (screenWidth/2) + moon.size.width, y: screenHeight/2)
		moon.zPosition = -199
		self.addChild(moon)

		loopingBg.position = CGPoint(x: 0, y: loopingBg.size.height/2)
		loopingBg.zPosition = -200
		self.addChild(loopingBg)

		loopingBg2.position = CGPoint(x: loopingBg2.size.width, y: loopingBg.size.height/2)
		loopingBg2.zPosition = -200
		self.addChild(loopingBg2)
		self.startLoopBackground()

		// Ground
		let groundTexture = SKTexture(imageNamed: "rocky_ground")
		let groundSize = CGSize(width:self.view!.bounds.width, height:groundTexture.size().height)
		ground = SKSpriteNode(texture: groundTexture, color: SKColor.clearColor(), size: groundSize)
		ground.name = "ground"
		if (isPhone == true) {
			ground.position = CGPoint(x: 0, y: 0)
		} else {
			ground.position = CGPoint(x: 0, y: groundSize.height/2)
		}
		ground.zPosition = 10
		ground.physicsBody = SKPhysicsBody(rectangleOfSize: groundSize)
		ground.physicsBody!.dynamic = false
		ground.physicsBody!.categoryBitMask = BodyType.ground.rawValue
		ground.physicsBody!.contactTestBitMask = BodyType.enemyMissile.rawValue | BodyType.enemyBomb.rawValue
		self.addChild(ground)

		// PlayerBase
		playerBase.name = "playerBase"
		playerBase.position = CGPoint(x: 0, y: ground.position.y + (playerBase.size.height/2))
		playerBase.zPosition = 5
		playerBase.physicsBody = SKPhysicsBody(circleOfRadius: playerBase.size.width/2)
		playerBase.physicsBody!.dynamic = false
		playerBase.physicsBody!.categoryBitMask = BodyType.playerBase.rawValue
		self.addChild(playerBase)

		// Turret
		turret.name = "turret"
		turret.position = CGPoint(x: 0, y: playerBase.position.y)
		turret.zPosition = 6
		turret.anchorPoint = CGPoint(x: 0.5, y: 0.0)
		self.addChild(turret)

		// Target

		target.position = CGPoint(x: turret.position.x, y: turret.size.height + 100)
		target.zPosition = 3
		self.addChild(target)
	}

	func startLoopBackground() {
		let moveStars = SKAction.moveByX(-loopingBg.size.width, y: 0, duration: 80)
		let moveStarsBack = SKAction.moveByX(loopingBg.size.width, y: 0, duration: 0)
		let starsSeq = SKAction.sequence([moveStars,moveStarsBack])
		loopingBg.runAction(SKAction.repeatActionForever(starsSeq))
		loopingBg2.runAction(SKAction.repeatActionForever(starsSeq))

		let moveMoon = SKAction.moveByX(-screenWidth * 1.3, y: 0, duration: 60)
		let moveMoonBack = SKAction.moveByX(screenWidth * 1.3, y: 0, duration: 0)
		let moonWait = SKAction.waitForDuration(20)
		let moonSeq = SKAction.sequence([moveMoon,moonWait,moveMoonBack])
		moon.runAction(SKAction.repeatActionForever(moonSeq))
	}

	// MARK: ======== START GAME

	func startGame() {
		gameIsActive = true

		// initialize enemy missile attack
		self.initiateEnemyMissiles()

		// add particles / dots behind enemy missiles
		self.initiateDotLoop()

		// initiate drones flying across

		// check to see if the game is over

		// clear out unseen objects
		self.clearOutOfSceneItems() // call this once, then it calls itself every second
	}

	func clearOutOfSceneItems() {
		self.clearBullets()

		// wait a sec, then call this function again
		let waitASec = SKAction.waitForDuration(1)
		let clearSceneBlock = SKAction.runBlock(self.clearOutOfSceneItems)
		self.runAction(SKAction.sequence([waitASec,clearSceneBlock]), withKey: "clearAction")
	}

	func clearBullets() {
		self.enumerateChildNodesWithName("bullet", usingBlock: {
			node, stop in
			//this code runs when we find a bullet
			if(node.position.x < -(self.screenWidth/2)) {
				node.removeFromParent()
			} else if(node.position.x > (self.screenWidth/2)) {
				node.removeFromParent()
			} else if(node.position.y > (self.screenHeight)) {
				node.removeFromParent()
			}


		})
	}

	func clearEnemyMissiles() {

	}

	// MARK: ======== ENEMY MISSILE

	func initiateEnemyMissiles() {
		let wait = SKAction.waitForDuration(2)
		let block = SKAction.runBlock(fireEnemyMissile)
		let seq = SKAction.sequence([block,wait])
		self.runAction(SKAction.repeatActionForever(seq), withKey: "enemyLaunchAction")
	}

	func fireEnemyMissile () {
		let missile = EnemyMissile()
		missile.createMissile("enemyMissile")
		let randomX = arc4random_uniform(UInt32(screenWidth))
		missile.position = CGPoint(x: CGFloat(randomX) - (screenWidth/2), y: screenHeight+50)
		self.addChild(missile)

		let randomVecX = arc4random_uniform(20)
		let vecX = CGFloat(randomVecX) / 10

		// Zero is in the middle of the screen
		if( missile.position.x < 0) {
			// missile is on the left side of the screen
			missile.physicsBody?.applyImpulse(CGVector(dx: vecX, dy: 0))
		} else {
			// missile is on the right side of the screen
			missile.physicsBody?.applyImpulse(CGVector(dx: -vecX, dy: 0))
		}
	}

	func initiateDotLoop() {
		let wait = SKAction.waitForDuration(1/60)
		let block = SKAction.runBlock(addDot)
		let seq = SKAction.sequence([block,wait])
		self.runAction(SKAction.repeatActionForever(seq), withKey: "dotAction")
	}

	func addDot() {
		let fade = SKAction.fadeAlphaBy(0.0, duration: 3)
		let grow = SKAction.scaleTo(3.0, duration: 3)
		let color = SKAction.colorizeWithColor(SKColor.redColor(), colorBlendFactor: 1, duration: 3)
		let group = SKAction.group([fade,grow,color])
		let remove = SKAction.removeFromParent()
		let seq = SKAction.sequence([group,remove])

		self.enumerateChildNodesWithName("enemyMissile", usingBlock: {
			node, stop in
			let dot = SKSpriteNode(imageNamed: "dot")
			dot.position = node.position
			dot.zPosition = -1
			dot.xScale = 0.3
			dot.yScale = 0.3

			dot.runAction(seq)
			self.addChild(dot)
		})
	}

	// MARK: ======== CONTROLS

	func rotatedView(sender:UIRotationGestureRecognizer) {
	//	let turret = self.childNodeWithName("turret") as! SKSpriteNode
		if(sender.state == .Changed) {
			turretRotation = CGFloat( -sender.rotation ) + turretOffset
			let maxRotation:CGFloat = 1.4
			if (turretRotation < -maxRotation) {
				turretRotation = -maxRotation
			} else if (turretRotation > maxRotation) {
				turretRotation = maxRotation
			}

			turret.zRotation = turretRotation
			target.zRotation = turretRotation

			let xDist:CGFloat = sin(turretRotation) * turretTargetRadius
			let yDist:CGFloat = cos(turretRotation) * turretTargetRadius
			target.position = CGPoint(x: turret.position.x - xDist, y: turret.position.y + yDist)
		}
		if(sender.state == .Ended) {
			turretOffset = turretRotation
		}
	}

	func tappedView() {
		self.playerBase.runAction(self.rattleAction)
		self.turret.runAction(self.rattleAction)
		self.fireBullet()
		self.playSound("gun1.caf")
	}

	func fireBullet() {
		let bullet:SKSpriteNode = SKSpriteNode(imageNamed: "bullet")
		let xDist:CGFloat = sin(self.turretRotation) * 70
		let yDist:CGFloat = cos(self.turretRotation) * 70
		let forceXDist:CGFloat = sin(self.turretRotation) * 250
		let forceYDist:CGFloat = cos(self.turretRotation) * 250
		let force:CGVector = CGVector(dx:self.turret.position.x - forceXDist, dy:self.turret.position.y + forceYDist)

		bullet.name = "bullet"
		bullet.position = CGPoint(x: self.turret.position.x - xDist, y: self.turret.position.y + yDist)
		bullet.zRotation = self.turretRotation
		bullet.physicsBody = SKPhysicsBody(circleOfRadius: bullet.size.width/3)
		bullet.physicsBody!.categoryBitMask = BodyType.bullet.rawValue

		self.addChild(bullet) // add bullet before applying force

		bullet.physicsBody!.applyForce(force)

		if let fireEmitter = SKEmitterNode(fileNamed: "FireParticles") {
			fireEmitter.position = CGPoint(x: self.turret.position.x, y: self.turret.size.height)
			fireEmitter.zPosition = 1
			fireEmitter.numParticlesToEmit = 25

			self.turret.addChild(fireEmitter)
		}
	}

	// MARK: ======== AUDIO / SFX

	func playBackgroundAudio(name:String) {
		let bgAudioUrlPath = NSBundle.mainBundle().pathForResource(name, ofType: "mp3")
		let bgAudioUrl = NSURL.fileURLWithPath(bgAudioUrlPath!)

		do {
			try bgAudioPlayer = AVAudioPlayer(contentsOfURL: bgAudioUrl)
			bgAudioPlayer.volume = 0.5 //half volume
			bgAudioPlayer.numberOfLoops = -1
			bgAudioPlayer.prepareToPlay()
			bgAudioPlayer.play()
		} catch {
			print("can't play background music")
		}

	}

	func playSound(name:String) {
		let sfx = SKAction.playSoundFileNamed(name, waitForCompletion: false)
		self.runAction(sfx)
	}

}
